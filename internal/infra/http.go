package infra

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/google/go-querystring/query"
	"go.uber.org/zap"
)

type HTTPResponseParser func(*http.Response) error

func JSONResponseParser(jsonResponse interface{}) HTTPResponseParser {
	return func(response *http.Response) error {
		buf, err := ioutil.ReadAll(response.Body)
		if err != nil {
			log.Println("Something went wrong when trying to read the response...")
			return err
		}

		err = json.Unmarshal(buf, &jsonResponse)
		return err
	}
}

func DoGETRequest(ctx context.Context, endpoint string, queryParams interface{}, timeoutInSeconds float64, parse HTTPResponseParser) error {
	querystring, err := encodeQueryParams(queryParams)
	if err != nil {
		Logger(ctx).Warn("Something went wrong when trying to encode the querystring...")
		return err
	}

	url := fmt.Sprintf("%s%s", endpoint, querystring)
	Logger(ctx).Info("Making a external request", zap.String("url", url))
	response, err := httpClient(timeoutInSeconds).Get(url)
	Logger(ctx).Info("Request was done", zap.String("url", url), zap.Int("status_code", response.StatusCode))
	if err != nil {
		Logger(ctx).Warn("Something went wrong when trying to make the request...")
		return err
	}

	if err = parse(response); err != nil {
		Logger(ctx).Warn("Couldn't parse the response body in the given interface...")
		return err
	}
	return nil
}

func httpClient(timeoutInSeconds float64) *http.Client {
	return &http.Client{Timeout: time.Second * time.Duration(timeoutInSeconds)}
}

func encodeQueryParams(queryParams interface{}) (string, error) {
	if queryParams == nil {
		return "", nil
	}

	values, err := query.Values(queryParams)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("?%s", values.Encode()), err
}
