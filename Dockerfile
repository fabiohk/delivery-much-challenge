FROM golang:alpine AS build-env

# Creates application directory
WORKDIR /app

# Copy required source to container
COPY . ./

# Build the project
RUN go build -o out/main .

FROM alpine:latest AS runtime-env

# Creates application directory
WORKDIR /app

# Creates an appgroup, appuser and change the ownership of the application's folder
RUN addgroup -S appgroup && \
    adduser -S appuser -G appgroup && \
    chown appuser /app

COPY --chown=appuser:appgroup --from=build-env /app/out /app

# Switching to the non-root appuser for security
USER appuser

ENTRYPOINT ["./main"]