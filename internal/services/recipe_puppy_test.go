package services

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestShouldSuccessfullySearchForRecipes(t *testing.T) {
	response, err := SearchRecipes(nil, nil)

	assert.Nil(t, err, "retrieved with no errors")
	assert.NotNil(t, response, "response isn't nil")
}
