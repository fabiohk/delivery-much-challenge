package api

import (
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestShouldSuccessfullyRetrieveIngredients(t *testing.T) {
	ingredientsQuery := "onion, tomato"

	ingredients, err := ParseIngredientsQuery(nil, ingredientsQuery)

	assert.Nil(t, err, "should be nil")
	assert.Equal(t, []string{"onion", "tomato"}, ingredients, "should be the same")
}

func TestShouldAccuseErrorWhenEmptyStringIsGiven(t *testing.T) {
	ingredients, err := ParseIngredientsQuery(nil, "")

	assert.Nil(t, ingredients, "should be nil")
	assert.Errorf(t, err, "no ingredients given")
}

func TestShouldAccuseErrorWhenMoreThanAllowedIngredientsAreGiven(t *testing.T) {
	ingredientsQuery := "onion, tomato, potato, carrot"

	ingredients, err := ParseIngredientsQuery(nil, ingredientsQuery)

	assert.Nil(t, ingredients, "should be nil")
	assert.Errorf(t, err, "you can't give more than 3 ingredients")
}

func TestShouldSuccessfullyRetrieveRecipes(t *testing.T) {
	ingredients := []string{"onion", "tomato"}

	response, err := RetrieveRecipes(nil, ingredients)

	assert.Nil(t, err, "should be nil")
	assert.Equal(t, []string{"onion", "tomato"}, response.Keywords, "should be the same")

	if assert.NotEmpty(t, response.Recipes) {
		recipe := response.Recipes[0]
		assert.True(t, sort.StringsAreSorted(recipe.Ingredients), "ingredients should be sorted")
		assert.NotNil(t, recipe.Title, "title shouln't be nil")
		assert.NotNil(t, recipe.Link, "link shouln't be nil")
		assert.NotNil(t, recipe.GIF, "gif shouldn't be nil")
	}
}
