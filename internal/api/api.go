package api

import (
	"errors"
	"sort"
	"strings"
	"sync"
    "fmt"

	"context"

	"delivery-much-challenge/configs"
	"delivery-much-challenge/internal/infra"
	"delivery-much-challenge/internal/services"

	"go.uber.org/zap"
)

type RecipeObject struct {
	Title       string   `json:"title"`
	Ingredients []string `json:"ingredients"`
	Link        string   `json:"link"`
	GIF         string   `json:"gif"`
}

type RecipesResponse struct {
	Keywords []string       `json:"keywords"`
	Recipes  []RecipeObject `json:"recipes"`
}

var maxIngredients = configs.MaxIngredients()

func ParseIngredientsQuery(ctx context.Context, ingredientsQuery string) ([]string, error) {
	ingredients := strings.Split(ingredientsQuery, ",")
	infra.Logger(ctx).Debug("Retrieved ingredients", zap.Strings("ingredients", ingredients))
	quantityOfGivenIngredients := len(ingredients)

	if ingredients[0] == "" {
		return nil, errors.New("no ingredients given")
	}

	if quantityOfGivenIngredients > maxIngredients {
		return nil, fmt.Errorf("you can't give more than %d ingredients", maxIngredients)
	}

	for i := range ingredients {
		ingredients[i] = strings.TrimSpace(ingredients[i])
	}

	return ingredients, nil
}

func RetrieveRecipes(ctx context.Context, ingredients []string) (RecipesResponse, error) {
	recipesQuery := services.RecipePuppySearchRecipesQuery{Ingredients: strings.Join(ingredients, ","), Page: 1}
	infra.Logger(ctx).Info("Searching recipes for ingredients", zap.Strings("ingredients", ingredients))
	response, err := services.SearchRecipes(ctx, &recipesQuery)

	if err != nil {
		return RecipesResponse{}, err
	}

	recipeGIFs, err := retrieveRecipeGIFs(ctx, response.Results)

	if err != nil {
		return RecipesResponse{}, err
	}

	var recipes []RecipeObject
	for idx, recipe := range response.Results {
		recipeTitle := strings.TrimSpace(recipe.Title)
		sortedIngredients := sortIngredients(recipe.Ingredients)
		recipes = append(recipes, RecipeObject{Title: recipeTitle, Ingredients: sortedIngredients, Link: recipe.Href, GIF: recipeGIFs[idx]})
	}

	return RecipesResponse{Keywords: ingredients, Recipes: recipes}, nil
}

func sortIngredients(ingredientsStr string) []string {
	ingredients := strings.Split(ingredientsStr, ",")
	for i := range ingredients {
		ingredients[i] = strings.TrimSpace(ingredients[i])
	}

	sort.Strings(ingredients)
	return ingredients
}

func retrieveRecipeGIFs(ctx context.Context, recipes []services.RecipePuppyRecipeObject) ([]string, error) {
	fatalErrors := make(chan error)
	wgDone := make(chan bool)

	var recipeGIFs = make([]string, len(recipes))
	var wg sync.WaitGroup

	for idx, recipe := range recipes {
		recipeTitle := strings.TrimSpace(recipe.Title)
		infra.Logger(ctx).Info("Searching a GIF for recipe", zap.String("recipe", recipeTitle))
		wg.Add(1)
		gifQuery := services.GiphySearchGIFsQuery{Query: recipeTitle, Limit: 1}
		go func(idx int) {
			defer func() {
				var err error
				if r := recover(); r != nil {
					switch x := r.(type) {
					case string:
						err = errors.New(x)
					case error:
						err = x
					default:
						err = errors.New("unknown panic")
					}
					fatalErrors <- err
				}
			}()
			defer wg.Done()

			response, err := services.SearchGIFs(ctx, &gifQuery)
			if err != nil {
				fatalErrors <- err
			}

			if len(response.Data) > 0 {
				recipeGIFs[idx] = response.Data[0].URL
			} else {
				recipeGIFs[idx] = ""
			}
		}(idx)
	}

	go func() {
		infra.Logger(ctx).Debug("Waiting for GIF searches conclusion...")
		wg.Wait()
		close(wgDone)
	}()

	select {
	case <-wgDone:
		break
	case err := <-fatalErrors:
		return nil, err
	}

	return recipeGIFs, nil
}
