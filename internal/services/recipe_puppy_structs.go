package services

type RecipePuppySearchRecipesQuery struct {
	Ingredients string `url:"i"`
	Page        int    `url:"p,omitempty"`
}

type RecipePuppyRecipeObject struct {
	Title       string `json:"title"`
	Href        string `json:"href"`
	Ingredients string `json:"ingredients"`
	Thumbnail   string `json:"thumbnail"`
}

type RecipePuppySearchRecipeObject struct {
	Title   string                    `json:"title"`
	Version float64                   `json:"version"`
	Href    string                    `json:"href"`
	Results []RecipePuppyRecipeObject `json:"results"`
}
