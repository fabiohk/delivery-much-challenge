package infra

import (
	"context"

	"go.uber.org/zap"
)

var logger *zap.Logger

func Logger(ctx context.Context) *zap.Logger {
	if logger == nil {
		logger = InitLogger()
	}

	newLogger := logger
	if ctx != nil {
		if ctxRequestID, ok := ctx.Value("request_id").(string); ok {
			newLogger = newLogger.With(zap.String("request_id", ctxRequestID))
		}
	}
	return newLogger
}

func InitLogger() *zap.Logger {
	logger, _ = zap.NewProduction()
	defer logger.Sync()
	zap.ReplaceGlobals(logger)
	return logger
}
