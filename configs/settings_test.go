package configs

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRecipePuppyHostShouldBeSet(t *testing.T) {
	os.Setenv("RECIPE_PUPPY_HOST", "https://recipe-puppy.net")

	got := RecipePuppyHost()

	assert.Equal(t, "https://recipe-puppy.net", got, "should be the same")
}

func TestRecipePuppySearchPathShouldBeSet(t *testing.T) {
	os.Setenv("RECIPE_PUPPY_SEARCH_PATH", "search")

	got := RecipePuppySearchPath()

	assert.Equal(t, "search", got, "should be the same")
}

func TestRecipePuppyTimeoutShouldBeSet(t *testing.T) {
	os.Setenv("RECIPE_PUPPY_TIMEOUT", "20.0")

	got := RecipePuppyTimeout()

	assert.Equal(t, 20.0, got, "should be the same")
}

func TestGiphyHostShouldBeSet(t *testing.T) {
	os.Setenv("GIPHY_HOST", "https://giphy.net")

	got := GiphyHost()

	assert.Equal(t, "https://giphy.net", got, "should be the same")
}

func TestGiphySearchPathShouldBeSet(t *testing.T) {
	os.Setenv("GIPHY_SEARCH_PATH", "v1/gifs/search")

	got := GiphySearchPath()

	assert.Equal(t, "v1/gifs/search", got, "should be the same")
}

func TestGiphyAPIKeyShouldBeSet(t *testing.T) {
	os.Setenv("GIPHY_API_KEY", "api_key")

	got := GiphyAPIKey()

	assert.Equal(t, "api_key", got, "should be the same")
}

func TestGiphyTimeoutShouldBeSet(t *testing.T) {
	os.Setenv("GIPHY_TIMEOUT", "42.0")

	got := GiphyTimeout()

	assert.Equal(t, 42.0, got, "should be the same")
}
