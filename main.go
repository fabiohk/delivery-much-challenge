package main

import (
	"fmt"
	"net/http"

	"delivery-much-challenge/configs"
	"delivery-much-challenge/internal"
	"delivery-much-challenge/internal/infra"

	"go.uber.org/zap"
)

func main() {
	infra.InitLogger()

	mux := http.NewServeMux()

	mux.HandleFunc("/recipes/", internal.RecipesHandler)

	logger := zap.S()

	serverAddress := fmt.Sprintf("%s:%d", configs.ServerHost(), configs.ServerPort())
	logger.Infof("Server is up and listening at %s", serverAddress)

	logger.Fatal(http.ListenAndServe(serverAddress, mux))
}
