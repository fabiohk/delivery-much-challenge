package internal

import (
	"context"
	"encoding/json"
	"net/http"

	"delivery-much-challenge/internal/api"
	"delivery-much-challenge/internal/infra"

	"github.com/google/uuid"
	"go.uber.org/zap"
)

type ErrorResponse struct {
	Message   string `json:"errorMsg"`
	RequestID string `json:"requestId"`
}

func RecipesHandler(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	ctx = WithRequestID(ctx, uuid.New().String())

	infra.Logger(ctx).Info("Received a request to retrieve recipes")

	var response interface{}
	var errorResponse *ErrorResponse
	var statusCode int

	ingredientsQuery := r.URL.Query().Get("i")
	ingredients, err := api.ParseIngredientsQuery(ctx, ingredientsQuery)
	if err != nil {
		infra.Logger(ctx).Error("The ingredients query didn't passed the validation!", zap.Error(err))
		statusCode = http.StatusBadRequest
		errorResponse = &ErrorResponse{Message: err.Error(), RequestID: ctx.Value("request_id").(string)}
	} else {
		recipesResponse, err := api.RetrieveRecipes(ctx, ingredients)
		if err != nil {
			infra.Logger(ctx).Error("Something wrong occurred when trying to fetch the recipes", zap.Error(err))
			statusCode = http.StatusInternalServerError
			errorResponse = &ErrorResponse{Message: "Unfortunately, something unexpected occurred, so we can't complete your request.", RequestID: ctx.Value("request_id").(string)}
		} else {
			response = recipesResponse
			statusCode = http.StatusOK
		}
	}

	w.WriteHeader(statusCode)
	if errorResponse != nil {
		json.NewEncoder(w).Encode(*errorResponse)
	} else {
		json.NewEncoder(w).Encode(response)
	}
	infra.Logger(ctx).Info("Request finished!", zap.String("method", r.Method), zap.String("path", r.URL.Path), zap.String("query", r.URL.RawQuery), zap.String("user_agent", r.UserAgent()), zap.Int("http_status", statusCode))
}
