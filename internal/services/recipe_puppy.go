package services

import (
	"context"
	"fmt"

	"delivery-much-challenge/configs"
	"delivery-much-challenge/internal/infra"
)

var (
	recipePuppyHost       = configs.RecipePuppyHost()
	recipePuppySearchPath = configs.RecipePuppySearchPath()
	recipePuppyTimeout    = configs.RecipePuppyTimeout()
)

func SearchRecipes(ctx context.Context, queryParams *RecipePuppySearchRecipesQuery) (*RecipePuppySearchRecipeObject, error) {
	endpoint := fmt.Sprintf("%s/%s", recipePuppyHost, recipePuppySearchPath)
	var jsonResponse RecipePuppySearchRecipeObject
	parser := infra.JSONResponseParser(&jsonResponse)

	err := infra.DoGETRequest(ctx, endpoint, queryParams, recipePuppyTimeout, parser)
	return &jsonResponse, err
}
