package configs

import (
	"log"

	"github.com/kelseyhightower/envconfig"
)

type Settings struct {
	RecipePuppyHost       string  `required:"true" split_words:"true"`
	RecipePuppySearchPath string  `required:"true" split_words:"true"`
	RecipePuppyTimeout    float64 `default:"10" split_words:"true"`
	GiphyHost             string  `required:"true" split_words:"true"`
	GiphyAPIKey           string  `required:"true" split_words:"true"`
	GiphySearchPath       string  `required:"true" split_words:"true"`
	GiphyTimeout          float64 `default:"10" split_words:"true"`
	ServerPort            int     `required:"true" split_words:"true"`
	ServerHost            string  `required:"true" split_words:"true"`
	MaxIngredients        int     `default:"3" split_words:"true"`
}

func RecipePuppyHost() string {
	settings := getSettings()
	return settings.RecipePuppyHost
}

func RecipePuppySearchPath() string {
	settings := getSettings()
	return settings.RecipePuppySearchPath
}

func RecipePuppyTimeout() float64 {
	settings := getSettings()
	return settings.RecipePuppyTimeout
}

func GiphyHost() string {
	settings := getSettings()
	return settings.GiphyHost
}

func GiphyTimeout() float64 {
	settings := getSettings()
	return settings.GiphyTimeout
}

func GiphyAPIKey() string {
	settings := getSettings()
	return settings.GiphyAPIKey
}

func GiphySearchPath() string {
	settings := getSettings()
	return settings.GiphySearchPath
}

func ServerHost() string {
	settings := getSettings()
	return settings.ServerHost
}

func ServerPort() int {
	settings := getSettings()
	return settings.ServerPort
}

func MaxIngredients() int {
	settings := getSettings()
	return settings.MaxIngredients
}

func getSettings() Settings {
	var settings Settings

	err := envconfig.Process("", &settings)
	if err != nil {
		log.Fatal(err.Error())
	}

	return settings
}
