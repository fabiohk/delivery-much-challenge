package services

import (
	"context"
	"fmt"

	"delivery-much-challenge/configs"
	"delivery-much-challenge/internal/infra"
)

var (
	giphyHost       = configs.GiphyHost()
	giphySearchPath = configs.GiphySearchPath()
	giphyTimeout    = configs.GiphyTimeout()
	giphyAPIKey     = configs.GiphyAPIKey()
)

func SearchGIFs(ctx context.Context, queryParams *GiphySearchGIFsQuery) (*GiphySearchGIFObject, error) {
	endpoint := fmt.Sprintf("%s/%s", giphyHost, giphySearchPath)
	var jsonResponse GiphySearchGIFObject
	parser := infra.JSONResponseParser(&jsonResponse)
	queryParams.APIKey = giphyAPIKey

	err := infra.DoGETRequest(ctx, endpoint, queryParams, giphyTimeout, parser)
	return &jsonResponse, err
}
