package services

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestShouldSuccessfullySearchForGIFs(t *testing.T) {
	query := GiphySearchGIFsQuery{Query: "something", Limit: 1}
	response, err := SearchGIFs(nil, &query)

	assert.Nil(t, err, "retrieved with no errors")
	assert.NotNil(t, response, "response isn't nil")
}
