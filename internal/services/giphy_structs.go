package services

type GiphySearchGIFsQuery struct {
	APIKey   string `url:"api_key"`
	Query    string `url:"q"`
	Limit    int    `url:"limit,omitempty"`
	Offset   int    `url:"offset,omitempty"`
	Rating   string `url:"ating,omitempty"`
	Language string `url:"lang,omitempty"`
	RandomID string `url:"random_id,omitempty"`
}

type GiphyUserObject struct {
	AvatarURL   string `json:"avatar_url"`
	BannerURL   string `json:"banner_url"`
	ProfileURL  string `json:"profile_url"`
	Username    string `json:"username"`
	DisplayName string `json:"display_name"`
}

type GiphyImageObject struct {
	URL      string `json:"url,omitempty"`
	Width    string `json:"width,omitempty"`
	Height   string `json:"height,omitempty"`
	Size     string `json:"size,omitempty"`
	MP4      string `json:"mp4,omitempty"`
	MP4Size  string `json:"mp4_size,omitempty"`
	Webp     string `json:"webp,omitempty"`
	WebpSize string `json:"webp_size,omitempty"`
	Frames   string `json:"frames,omitempty"`
}

type GiphyImagesObject struct {
	FixedHeight            GiphyImageObject `json:"fixed_height,omitempty"`
	FixedHeightStill       GiphyImageObject `json:"fixed_height_still,omitempty"`
	FixedHeightDownsampled GiphyImageObject `json:"fixed_height_downsampled,omitempty"`
	FixedWidth             GiphyImageObject `json:"fixed_width,omitempty"`
	FixedWidthStill        GiphyImageObject `json:"fixed_width_still,omitempty"`
	FixedWidthDownsampled  GiphyImageObject `json:"fixed_width_downsampled,omitempty"`
	FixedHeightSmall       GiphyImageObject `json:"fixed_height_small,omitempty"`
	FixedHeightSmallStill  GiphyImageObject `json:"fixed_height_small_still,omitempty"`
	FixedWidthSmall        GiphyImageObject `json:"fixed_width_small,omitempty"`
	FixedWidthSmallStill   GiphyImageObject `json:"fixed_width_small_still,omitempty"`
	Downsized              GiphyImageObject `json:"downsized,omitempty"`
	DownsizedStill         GiphyImageObject `json:"downsized_still,omitempty"`
	DownsizedLarge         GiphyImageObject `json:"downsized_large,omitempty"`
	DownsizedMedium        GiphyImageObject `json:"downsized_medium,omitempty"`
	DownsizedSmall         GiphyImageObject `json:"downsized_small,omitempty"`
	Original               GiphyImageObject `json:"original,omitempty"`
	OriginalStill          GiphyImageObject `json:"original_still,omitempty"`
	Looping                GiphyImageObject `json:"looping,omitempty"`
	Preview                GiphyImageObject `json:"preview,omitempty"`
	PreviewGIF             GiphyImageObject `json:"preview_gif,omitempty"`
}

type GiphyGIFObject struct {
	Type             string            `json:"type"`
	ID               string            `json:"id"`
	Slug             string            `json:"slug"`
	URL              string            `json:"url"`
	BitlyURL         string            `json:"bitly_url"`
	EmbedURL         string            `json:"embed_url"`
	Username         string            `json:"username"`
	Source           string            `json:"source"`
	Rating           string            `json:"rating"`
	ContentURL       string            `json:"content_url"`
	User             GiphyUserObject   `json:"user"`
	SourceTLD        string            `json:"source_tld"`
	SourcePostURL    string            `json:"source_post_url"`
	UpdateDatetime   string            `json:"update_datetime"`
	CreateDatetime   string            `json:"create_datetime"`
	ImportDatetime   string            `json:"import_datetime"`
	TrendingDatetime string            `json:"trending_datetime"`
	Images           GiphyImagesObject `json:"images"`
	Title            string            `json:"title"`
}

type GiphyPaginationObject struct {
	Offset     int `json:"offset"`
	TotalCount int `json:"total_count"`
	Count      int `json:"count"`
}

type GiphyMetaObject struct {
	Message    string `json:"msg"`
	Status     int    `json:"status"`
	ResponseID string `json:"response_id"`
}

type GiphySearchGIFObject struct {
	Data       []GiphyGIFObject      `json:"data"`
	Pagination GiphyPaginationObject `json:"pagination"`
	Metadata   GiphyMetaObject       `json:"meta"`
}
