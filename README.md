# Delivery Much Challenge

A Golang solution to the [Delivery Much Tech Challenge](https://github.com/delivery-much/challenge)!

All the setup was made with [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/), so that you don't need to install locally any version of [Go](https://golang.org/).

## How to run the tests?

The unit tests can be run with:

```bash
docker-compose up tests
```

## How to run the app?

Run the service `app`:

```bash
docker-compose up app
```

And the service will start to run on localhost, using the port `8080`.

## Project settings

Here are the project settings that can be configured if needed (some of them are set on the [`.env`](.env) file):

|Settings|Description|Required?|Default value|
|--------|-----------|---------|-------------|
|SERVER_HOST|The host where the server will run|Yes|N.A.|
|SERVER_PORT|The host port that the server will receive requests|Yes|N.A.|
|MAX_INGREDIENTS|Defines how many ingredients the API can receive in the query|No|3 ingredients|
|RECIPE_PUPPY_HOST|[Recipe Puppy](http://www.recipepuppy.com/about/api/) host|Yes|N.A.|
|RECIPE_PUPPY_SEARCH_PATH|Recipe Puppy path used to search recipes|Yes|N.A.|
|RECIPE_PUPPY_TIMEOUT|Defines the timeout period (in seconds) for any request to Recipe Puppy|No|10 seconds|
|GIPHY_HOST|[Giphy API](https://developers.giphy.com/docs/) host|Yes|N.A.|
|GIPHY_SEARCH_PATH|Giphy path used to search GIFs|Yes|N.A.|
|GIPHY_API_KEY|The Giphy API Key used in the Giphy APIs|Yes|N.A.|
|GIPHY_TIMEOUT|Defines the timeout period (in seconds) for any request to Giphy|No|10 seconds|