package internal

import (
	"context"

	"testing"

	"github.com/stretchr/testify/assert"
)

func TestShouldSetRequestIdToContext(t *testing.T) {
	ctx := context.Background()
	requestID := "abc"

	ctx = WithRequestID(ctx, requestID)

	ctxRequestID, ok := ctx.Value("request_id").(string)
	assert.True(t, ok, "should be true")
	assert.Equal(t, "abc", ctxRequestID, "should be the same")
}
